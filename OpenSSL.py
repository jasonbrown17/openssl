#!/usr/bin/python
'''

  Generate a users private key, public key, and certificate signing request
  which is then signed by a certificate authority. Script uses
  Python ver. 2 for backward compatibility with Mac OS.

'''
__author__  = "Jason Brown"
__email__   = 'jason@jasonbrown.us'
__version__ = '0.7'
__status__  = 'Production'
__date__    = '20180621'


import string, sys, getpass
from subprocess import call

'''
  Main function which is used to call additional functions within the script
'''

def main():

  if len(sys.argv) < 2:
    print ('Type ./opensslcerts.py --help for options')
    sys.exit()
  if sys.argv[1].startswith('--'):
    option = sys.argv[1][2:]
    if option == 'help':
      print ('Usage ./opensslcerts.py [option]\n\
      --genecsr\n\
      --gensig\n\
      --sign\n\
      --version')
    elif option == 'gencsr':
      gencsr()
    elif option == 'genpem':
      genpem()
    elif option == 'sign':
      sign()
    elif option == 'version':
      version()
    else:
      print (sys.exit(2))	
  else:
    print ('No value given')

def gencsr():

  '''
    The gencsr function is used to generate the certificate key and certificate signing request. Unless otherwise
    specified, the default key length is 2048 bits however the user gets to choose any key length they choose.
  '''

  print 'Generate user keys and certificate signing request\n'
  KEYSIZE = raw_input('Key length [2048]: ')
  KEYNAME = raw_input('Name of key [first initial and last name]: ')
  PRIVKEY_NAME = KEYNAME + '.priv.key.pem'
  PUBKEY_NAME = KEYNAME + '.pub.key.pem'
  CSRNAME = KEYNAME + '.csr.pem'

  if KEYSIZE:
    pass
  else:
    KEYSIZE = 2048

  call('openssl genrsa -aes256 -out %s %s' % (PRIVKEY_NAME, KEYSIZE), shell=True)
  call('openssl rsa -in %s -out %s -outform PEM -pubout' % (PRIVKEY_NAME, PUBKEY_NAME), shell=True)

  call('openssl req -config OpenSSL.cnf -key %s -new -sha256 -out %s' % (PRIVKEY_NAME, CSRNAME), shell=True)

  print 'Submit CSR to a Certificate Authority'

def genpem():
  '''
    The genpem function is used if you receive a CER file from the certificate authority. This will
    convert the CER to PEM format.
  '''
  CER = raw_input('Name of CER file: ')
  PRIV = raw_input('Name of private key: ')
  PEM = CERT + 't.pem'

  print 'Convert CERT to PEM file'
  call('openssl x509 -inform DER -in %s -out %s' % (CER, PEM), shell=True)


def sign():

  '''
    Sign a file and verify the signature is correct
  '''

  FILE = raw_input('Name of file to sign: ')
  PRIV = raw_input('Name of private key: ')
  PUB = raw_input('Name of public key: ')
  SHAFILE = FILE + '.sha256'

  print 'Signing the file'
  call('openssl dgst -sha256 -sign %s -out %s %s' % (PRIV, SHAFILE, FILE), shell=True)

  print 'Verifying signature'
  call('openssl dgst -sha256 -verify %s -signature %s %s' % (PUB, SHAFILE, FILE), shell=True)


def version():
  '''
    Displays the version of the script
  '''
  print 'Version 0.7'


if __name__ == '__main__':
	main()
